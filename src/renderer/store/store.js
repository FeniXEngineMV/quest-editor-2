function Store () {
  let _projectDirectory = ''

  let _questList = []

  const _recentlyOpened = []

  const _config = {
    general: {
      maxRecentlyOpened: 5,
      theme: 'light'
    },
    list: {
      maxDescriptionCharacters: 15
    }
  }

  function _addToRecents (filepath) {
    if (_recentlyOpened.length <= _config.general.maxRecentlyOpened) {
      _recentlyOpened.push(filepath)
    }
  }

  return {
    set projectDirectory (filepath) {
      _addToRecents(filepath)
      _projectDirectory = filepath
    },

    get projectDirectory () {
      return _projectDirectory
    },

    set questList (array) {
      _questList = array
    },

    get questList () {
      return _questList
    },

    get config () {
      return _config
    },

    newQuest () {
      _questList.push({
        /* Add +1 so the UI reflects 1 for first quest and not 0 */
        id: _questList.length + 1 || 1,
        name: 'New Quest',
        location: '',
        giverName: '',
        iconid: 87,
        category: '',
        onStart: null,
        onComplete: null,
        onFail: null,
        description: 'Description',
        steps: [],
        rewards: [],
        costs: [],
        requirements: []
      })
    },

    removeQuest (id) {
      const index = _questList.findIndex((quest) => quest.id === id)
      _questList.splice(index, 1)
    }
  }
}
export const store = Store()
