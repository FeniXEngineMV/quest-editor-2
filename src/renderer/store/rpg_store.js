/* A basic store object to keep internal state */
// import {store} from './store'
const fs = require('fs')
const path = require('path')

function _rpgStore () {
  let _directory = ''
  let _dataPath = '/data/'

  const _Data = {}

  let _requiredData = ['System', 'CommonEvents', 'Weapons', 'Armors', 'Items', 'Skills']

  // const _createData = (data) => {
  //   data.forEach(value => value.forEach(key => {
  //     _Data[key] = value[key]
  //   }))
  // }

  const _filterRequiredData = files => files.filter(name => {
    return _requiredData.includes(name.replace('.json', ''))
  })

  function _readDir (path) {
    return new Promise((resolve, reject) => {
      fs.readdir(path, (err, files) => {
        if (err) {
          reject(err)
        }
        resolve(files)
      })
    })
  }

  function _readFile (file) {
    return new Promise((resolve, reject) => {
      fs.readFile(_dataPath + file, 'utf8', (err, data) => {
        if (err) {
          reject(err)
        }
        resolve(data)
      })
    })
  }

  async function _readJson (file) {
    _readFile(file)
    .then(data => {
      return { [file]: JSON.parse(data) }
    })
  }

  function _mapRequiredData (files) {
    files = _filterRequiredData(files)
    return files.map(file => _readJson(file)
    .then(data => console.log(data)))
  }

  return {
    set directory (filepath) {
      _directory = filepath
      _dataPath = path.join(filepath, '/data/')
    },

    get directory () {
      return _directory
    },

    get data () {
      if (_Data) {
        return _Data
      } else {
        console.error('RPG Data not loaded')
      }
    },

    loadData () {
      return new Promise((resolve, reject) => {
        _readDir(_dataPath).then(files => {
          console.log(_mapRequiredData(files))
        })
      })
    }
  }
  // End Of Store
}

export const rpgStore = _rpgStore()
