import Vue from 'vue'
import Router from 'vue-router'
import {store} from './../store/store'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'getting-started',
      component: require('@/components/GettingStarted.vue'),
      beforeRouteLeave: (to, from, next) => {
        if (store.projectDirectory === '') {
          next(false)
        } else {
          next()
        }
      }
    },
    {
      path: '/editor',
      name: 'editor',
      component: require('@/components/Editor')
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
